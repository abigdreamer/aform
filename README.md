# AForm 

告别繁琐的Ajax请求代码，纯标签也能享受异步无刷新更新数据。

[![star](https://gitee.com/yihrmc/aform/badge/star.svg?theme=dark)](https://gitee.com/yihrmc/aform/stargazers)



#### 项目介绍
AForm是一套页面的数据交互框架，通过标签属性来代替更多JS与后台数据交互操作，更深层次封装了Ajax。低侵入式，允许您更多的自定义操作。


#### 如何开始使用AForm？
下载、拷贝、导入AForm.js文件，开始使用！

[阅读项目Wiki，了解AForm。](https://gitee.com/yihrmc/aform/wikis/pages)


#### 是否兼容旧浏览器?
需要 **旧版jQuery库** ，jQuery()方法，实现兼容旧浏览器查询Element。
- 不兼容浏览器时，不依赖jQuery。
- 当浏览器不支持 **querySelectorAllf()** 方法时，会自动寻找jQuery对象!
- Jquery从2.x版本开始已经不再进行IE低版本(IE6、IE7、IE8)的兼容性处理。

#### 为什么无法使用自动化MD5(数据摘要)功能?
使用此功能，需要另导入MD5.js文件，实现md5算法。
1. 在AForm.js之前，导入MD5.js文件
2. 在使用前，实现 function aui_hex_md5(text) {} ,AFrom将会调用此方法实现MD5摘要功能。


#### 演示

[基于AForm的书籍搜索示例](http://aform.stu.red/aform/example/e1.html)

[v1.2 示例-AForm查询](http://aform.stu.red/aform/example/%E7%A4%BA%E4%BE%8B-AForm%20form%E7%BB%84%E4%BB%B6.html)

### 温馨提示:1.2向下兼容1.1，请尽早升级。Wiki教程将更新1.2的新特性，新教程示例将全面使用aformv1.2.js。

### ------------------------------------------------------

