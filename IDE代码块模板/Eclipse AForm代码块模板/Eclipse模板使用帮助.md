### Eclipse AForm代码块 模板帮助

[下载eclipse_aform_html_templates.xml](https://yihrmc.gitee.io/aform/download/eclipse_aform_html_templates.xml)

0.下载 **eclipse_aform_html_templates.xml** (AForm Eclipse模板文件) 文件，到电脑中。

1.打开Eclipse，在菜单栏中依次寻找: Window > Preferences > Web > HTML Files > Editor > Templates。

![Eclipse模板导入说明](https://images.gitee.com/uploads/images/2018/1016/142017_7dc2f154_438681.png "Eclipse模板导入说明.png")

2.在Templates页面中，找到 `Import...` 按钮，点击导入下载的 **eclipse_aform_html_templates.xml** (AForm Eclipse模板文件)

3.文件导入完成后，点击 Apply 或 Apply and Close 应用即可生效。


