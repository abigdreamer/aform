
with_defaults :scope => 'text.html entity.other.attribute-name.html' do |bundle|  #=====HTML属性代码块====================================================

  #AForm aform组件
  snippet 'aui-aform' do |s|
    s.trigger = 'aui-aform'
    s.expansion='aui-aform="${1:/next/last/parent/children}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AForm aform组件 end
  
  #AForm查询
  snippet 'aui-scope' do |s|
    s.trigger = 'aui-scope'
    s.expansion='aui-scope="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-for' do |s|
    s.trigger = 'aui-for'
    s.expansion='aui-for="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AForm查询 end
  
  #AFrom form组件
  snippet 'aui-action' do |s|
    s.trigger = 'aui-action'
    s.expansion='aui-action="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-method' do |s|
    s.trigger = 'aui-method'
    s.expansion='aui-method="${1:GET/POST}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-async' do |s|
    s.trigger = 'aui-async'
    s.expansion='aui-async="${1:true/false}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-result' do |s|
    s.trigger = 'aui-result'
    s.expansion='aui-result="${1:/none/remove:/function:/page:/reload/view:}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-event' do |s|
    s.trigger = 'aui-event'
    s.expansion='aui-event="${1:/now/change/click/dblclick/abort/blur/error/focus/keydown/keypress/keyup/load/mousedown/mousemove/mouseout/mouseover/mouseup/reset/resize/select/submit/unload}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AFrom form组件 end
  
  #AFrom form组件子标签
  snippet 'aui-input' do |s|
    s.trigger = 'aui-input'
    s.expansion='aui-input="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
   
  snippet 'aui-value' do |s|
    s.trigger = 'aui-value'
    s.expansion='aui-value="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
   
  snippet 'aui-attrValue' do |s|
    s.trigger = 'aui-attrValue'
    s.expansion='aui-attrValue="${1:/value/src/href/checked}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AFrom form组件子标签 end
   
  #AFrom form组件 数据格式属性
  snippet 'aui-notnull' do |s|
    s.trigger = 'aui-notnull'
    s.expansion='aui-notnull="${1:true/false}"'
    s.locationType='HTML_ATTRIBUTE'
  end
   
  snippet 'aui-format' do |s|
    s.trigger = 'aui-format'
    s.expansion='aui-format="${1:/none/number}"'
    s.locationType='HTML_ATTRIBUTE'
  end
   
  snippet 'aui-md5' do |s|
    s.trigger = 'aui-md5'
    s.expansion='aui-md5="${1:md5}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AFrom form组件 数据格式属性 end
  
  #AFrom view组件
  snippet 'aui-view' do |s|
    s.trigger = 'aui-view'
    s.expansion='aui-view="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-switch' do |s|
    s.trigger = 'aui-switch'
    s.expansion='aui-switch="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-case' do |s|
    s.trigger = 'aui-case'
    s.expansion='aui-case="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-if' do |s|
    s.trigger = 'aui-if'
    s.expansion='aui-if="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-each' do |s|
    s.trigger = 'aui-each'
    s.expansion='aui-each="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-item' do |s|
    s.trigger = 'aui-item'
    s.expansion='aui-item="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-text' do |s|
    s.trigger = 'aui-text'
    s.expansion='aui-text="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-html' do |s|
    s.trigger = 'aui-html'
    s.expansion='aui-html="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-run' do |s|
    s.trigger = 'aui-run'
    s.expansion='aui-run="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  
  snippet 'aui-data-' do |s|
    s.trigger = 'aui-data-'
    s.expansion='aui-data-${1:class/id/title/style}="$2"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AFrom view组件 end
  
  #AFrom meta组件
  snippet 'aui-auto' do |s|
    s.trigger = 'aui-auto'
    s.expansion='aui-auto="${1:var/cookie/session/local}"'
    s.locationType='HTML_ATTRIBUTE'
  end
  #AFrom meta组件 end
  
end