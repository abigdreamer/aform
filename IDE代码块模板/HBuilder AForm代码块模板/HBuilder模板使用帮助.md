### HBuilder AForm代码块 模板帮助

[下载hbuilder_aform_html_templates.rb](https://yihrmc.gitee.io/aform/download/hbuilder_aform_html_templates.rb)

0.下载 **hbuilder_aform_html_templates.rb** (AForm HBuilder模板文件) 文件，到电脑中。

1.打开HBuilder，在菜单中依次寻找: 工具(T) > 扩展代码块(B) > 自定义html代码块。

2.在弹出的 **html_snippets.rb** 文件中，将下载的 **hbuilder_aform_html_templates.rb** (AForm HBuilder模板文件) 文件 的内容，复制到该文件末尾处。

3.保存  **html_snippets.rb**  文件修改，即可生效。